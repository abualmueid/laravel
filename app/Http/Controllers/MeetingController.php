<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeetingController extends Controller
{
    public function create($a, $b, Request $request)
    {
        echo "Hi";
        echo $a;
        echo $b;
        echo $request->name;
        echo $request->age;
    }

    public function edit($meetingID)
    {
        $data['mid'] = $meetingID;
        return view('meeting', $data);
    }
}
