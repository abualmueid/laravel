<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', [StudentController::class, 'index']);
Route::get('/home/meeting/{roomNo}/edit/{id1}', [MeetingController::class, 'create']);
Route::get('/home/zoom/{meetingID}/edit', [MeetingController::class, 'edit']);

Route::get('/home', [AuthController::class, 'show']);
Route::get('/main', [AuthController::class, 'main']);
Route::get('/signup', [AuthController::class, 'signup']);
Route::get('/login', [AuthController::class, 'login']);
